# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Arc Menu package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Arc Menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-19 15:14+0100\n"
"PO-Revision-Date: 2017-07-15 02:09+0300\n"
"Last-Translator: Alice Charlotte Liddell <e-liss@tuta.io>\n"
"Language-Team: \n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.9\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: prefs.js:42
msgid "General"
msgstr ""

#: prefs.js:54
msgid "Behaviour"
msgstr "Поведение"

#: prefs.js:63
msgid "Disable activities hot corner"
msgstr "Отключить горячий угол активностей"

#: prefs.js:85
msgid "Set menu hotkey"
msgstr "Горячая клавиша меню"

#: prefs.js:91
msgid "Undefined"
msgstr "Неопределено"

#: prefs.js:92
msgid "Left Super Key"
msgstr "Левый Super"

#: prefs.js:93
msgid "Right Super Key"
msgstr "Правый Super"

#: prefs.js:105
msgid "Enable custom menu keybinding"
msgstr "Включить другое сочетание клавиш"

#: prefs.js:112
msgid "Syntax: <Shift>, <Ctrl>, <Alt>, <Super>"
msgstr "Синтаксис: <Shift>, <Ctrl>, <Alt>, <Super>"

#: prefs.js:153
msgid "Button appearance"
msgstr "Внешний вид кнопки"

#: prefs.js:165
msgid "Text for the menu button"
msgstr "Текст кнопки меню"

#: prefs.js:171
msgid "System text"
msgstr "Системный текст"

#: prefs.js:174
msgid "Custom text"
msgstr "Другой текст"

#: prefs.js:198
msgid "Set custom text for the menu button"
msgstr "Пользовательский текст кнопки меню"

#: prefs.js:218
msgid "Enable the arrow icon beside the button text"
msgstr "Включить стрелку возле текста кнопки"

#: prefs.js:240
msgid "Select icon for the menu button"
msgstr "Выберите значок кнопки меню"

#: prefs.js:250
msgid "Please select an image icon"
msgstr "Пожалуйста, выберите файл значка"

#: prefs.js:255
msgid "Arc Menu Icon"
msgstr "Значок Arc Menu"

#: prefs.js:256
msgid "System Icon"
msgstr "Системный значок"

#: prefs.js:257
msgid "Custom Icon"
msgstr "Другой значок"

#: prefs.js:283
msgid "Icon size"
msgstr "Размер значка"

#: prefs.js:283
msgid "default is"
msgstr "по умолчанию"

#: prefs.js:323
msgid "Appearance"
msgstr "Внешний вид"

#: prefs.js:332
msgid "Customize menu button appearance"
msgstr "Настроить отображение кнопки меню"

#: prefs.js:342
msgid "Icon"
msgstr "Значок"

#: prefs.js:343
msgid "Text"
msgstr "Текст"

#: prefs.js:344
msgid "Icon and Text"
msgstr "Значок и текст"

#: prefs.js:345
msgid "Text and Icon"
msgstr "Текст и значок"

#: prefs.js:370
msgid "Menu position in panel"
msgstr "Расположение меню на панели"

#: prefs.js:377
msgid "Left"
msgstr "Слева"

#: prefs.js:380
msgid "Center"
msgstr "По центру"

#: prefs.js:384
msgid "Right"
msgstr "Справа"

#: prefs.js:461
msgid "About"
msgstr "О программе"

#: prefs.js:490
msgid "Arc-Menu"
msgstr "Arc-Menu"

#: prefs.js:495
msgid "version: "
msgstr "версия:"

#: prefs.js:503
msgid "GitLab Page"
msgstr ""

#: menu.js:272
msgid "Home"
msgstr "Домашняя папка"

#: menu.js:456 menu.js:461
msgid "Software"
msgstr "Программное Обеспечение"

#: menu.js:466
msgid "Settings"
msgstr "Настройки"

#: menu.js:471
msgid "Tweak Tool"
msgstr "Tweak Tool"

#: menu.js:476
#, fuzzy
msgid "Tweaks"
msgstr "Tweak Tool"

#: menu.js:481
msgid "Terminal"
msgstr ""

#: menuWidgets.js:86
msgid "Activities Overview"
msgstr "Обзор активностей"

#: menuWidgets.js:199
msgid "Power Off"
msgstr "Выключение"

#: menuWidgets.js:212
msgid "Log Out"
msgstr "Выход из системы"

#: menuWidgets.js:225
msgid "Suspend"
msgstr "Приостановить работу ПК"

#: menuWidgets.js:243
msgid "Lock"
msgstr "Заблокировать экран"

#: menuWidgets.js:265
msgid "Back"
msgstr "Назад"

#: menuWidgets.js:483
msgid "Favorites"
msgstr "Избранное"

#: menuWidgets.js:616
msgid "Type to search…"
msgstr "Поиск..."

#: menuWidgets.js:781
msgid "Applications"
msgstr "Приложения"

#~ msgid "Webpage"
#~ msgstr "веб-страница"
